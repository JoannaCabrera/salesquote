const express = require('express');
const companyController = require('../controllers/companyCtrl');
const router = express.Router();

/* Add Product to the Database */
router.post('/add', (req, res) => {
	companyController.addCompany(req.body).then(result => res.send(result));
})

/* View All Materials from the Database */
router.get('/view/all', (req, res) => {
	companyController.viewAllCompanies().then(result => res.send(result));
})

/* View Supplier Materials from the Database */
router.get('/view/:companyId', (req, res) => {
	companyController.viewCompany(req.params).then(result => res.send(result));
})


module.exports = router;