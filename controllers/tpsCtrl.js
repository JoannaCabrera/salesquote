const TPS = require('../models/mod_tps');

// Add a TPS to the database
module.exports.addTps = (reqBody) => {
	let newTPS = new TPS ({
		code: reqBody.code,
		vendorAccount : reqBody.vendorAccount
	})

	return newTPS.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the currency in the database!`;
		} else {
			return `Successfully added the currency in the database!`;
		}
	})

}

// View All TPS from the database
module.exports.viewAllTps = () => {
	return TPS.find().then(result => {
		return result;
	})
}

// View a Specific TPS from the database
module.exports.viewTps = (reqParams) => {
	return TPS.findOne({code: reqParams.code}).then(result => {
		if(Object.keys(result).length < 1) {
			return `No TPS with a code of ${reqParams.code}`;
		} else {
			return result
		}
	})
}
