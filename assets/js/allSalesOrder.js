// Global Variables
let token = localStorage.getItem("token") 
let employeeId =localStorage.getItem("employeeId")
let isApprover = localStorage.getItem("isApprover")

//Logout Function 
function logoutFunction() {
    localStorage.clear();
    window.location.replace('./logout.html')
}

document.getElementById('userIcon').onclick = function() {
    logoutFunction();
}

// Filter Status
let filterStatus = document.getElementById("filterStatus");

if (localStorage.getItem("isApprover") === "true"){
    filterStatus.innerHTML = 
    `
    <option value="allStatusApprover" id="allStatusApprover">All Status</option>
    <option value="forApprovalStatusApprover" id="forApprovalStatusApprover">For Approval</option>
    <option value="approvedStatusApprover" id="approvedStatusApprover">Approved</option>
    <option value="rejectedStatusApprover" id="rejectedStatusApprover">Rejected</option>
    `
}

if (localStorage.getItem("isRequestor") === "true"){
    filterStatus.innerHTML = 
    `
    <option value="allStatusRequestor" id="allStatusRequestor">All Status</option>
    <option value="inPreparationStatusRequestor" id="inPreparationStatusRequestor">In Preparation</option>
    <option value="forApprovalStatusRequestor" id="forApprovalStatusRequestor">For Approval</option>
    <option value="postedStatusRequestor" id="postedStatusRequestor">Posted</option>
    <option value="rejectedStatusRequestor" id="rejectedStatusRequestor">Rejected</option>
    <option value="cancelStatusRequestor" id="cancelStatusRequestor">Cancelled</option>
   `
}

// Buttons
let buttonContainerList = document.getElementById("buttonRow")

if(localStorage.getItem("isApprover") === "true") {
	buttonContainerList.innerHTML = 
	`
	<button id="viewButtonApprover">View</button>
    <button id="approveButton">Approve</button>
    <button id="rejectButton">Reject</button>
	<button id="closeButton">Close</button>
	`

    document.getElementById('viewButtonApprover').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        viewFunctionApprover()
    }

    document.getElementById('approveButton').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        approveFunction()
    }

    document.getElementById('rejectButton').onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        rejectFunction()
    }
    document.getElementById("closeButton").onclick = function(e) {
        e.preventDefault()
        closeFunction()
    }
} 


// View Function Approver
function viewFunctionApprover() {
    if(selectedSalesOrders < 1) {
        alert('Please Select a Sales Order')
    } else if(selectedSalesOrders.length > 1) {
        selectedSalesOrders.length = 0
        alert('Action cannot be performed on multiple line items. Please select a single line item')
    } else {
        let docId = selectedSalesOrders[0].salesOrderNo
        window.location.replace(`./salesOrderViewing.html?documentId=${docId}`)
    }
}

// Approve Function
function approveFunction() {
    if(selectedSalesOrders < 1) {
        alert('Please Select a Sales Order')
    } else {
        for(let i = 0; i < selectedSalesOrders.length; i++) {
            if(selectedSalesOrders[i].docStatus !== 'For Approval') {
                continue;
            } else {
                fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/salesOrderMonitoring/approve/${selectedSalesOrders[i].salesOrderNo}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
            }
        }
        alert('Successfully Approved the Sales Order')	
        selectedSalesOrders.length = 0
        window.location.reload()
    }
}

// Reject Function
function rejectFunction() {
    if(selectedSalesOrders < 1) {
        alert('Please Select a Sales Order')
    } else {
        for(let i = 0; i < selectedSalesOrders.length; i++) {
            if(selectedSalesOrders[i].docStatus !== 'For Approval') {
                continue;
            } else {
                fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/salesOrderMonitoring/reject/${selectedSalesOrders[i].salesOrderNo}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
            }
        }
        alert('Successfully Rejected the Sales Order')	
        selectedSalesOrders.length = 0
        window.location.reload()
    }
}

// Close Function
function closeFunction(){
    window.location.replace('./launchpad.html')
}

if(localStorage.getItem("isRequestor") === "true") {
	buttonContainerList.innerHTML =
	`
    <button id="newButton">New</button>
    <button id="viewButtonRequestor">View</button>
    <button id="closeButton">Close</button>
	`

    document.getElementById("newButton").onclick = function(e) {
        e.preventDefault()
        newFunction()
    }

    document.getElementById("viewButtonRequestor").onclick = function(e) {
        e.preventDefault()
        getValuesOfCheckedBox()
        viewFunctionRequestor()
    }

    document.getElementById("closeButton").onclick = function(e) {
        e.preventDefault()
        closeFunction()
    }
}

// New Function
function newFunction(){
    location.replace("./salesOrderCreation.html")
}
        
// View Function Requestor
function viewFunctionRequestor() {
    if(selectedSalesOrders.length < 1) {
        alert('Please Select a Sales Order')
    } else if(selectedSalesOrders.length > 1) {
        selectedSalesOrders.length = 0
        alert('Action cannot be performed on multiple line items. Please select a single line item')
    } else {
        let docId = selectedSalesOrders[0].salesOrderNo
        window.location.replace(`./salesOrderViewing.html?documentId=${docId}`)
    }
}


// Close Function
function closeFunction(){
	window.location.replace('./launchpad.html')
}

// Select All Checkbox
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}

let selectedSalesOrders = []

// Select a Checkbox
function getValuesOfCheckedBox() {
    //Reference the Table.
    let grid = document.getElementById("salesOrderTable");

    //Reference the CheckBoxes in Table.
    let checkBoxes = grid.getElementsByTagName("INPUT");

    //Loop through the CheckBoxes.
    for (let i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
            let row = checkBoxes[i].parentNode.parentNode
            let data = {
                salesOrderId: row.cells[1].innerHTML,
                salesQuoteNo: row.cells[2].innerText,
                accountId: row.cells[3].innerHTML,
                accountName: row.cells[4].innerHTML,
                salesGroup:  row.cells[5].innerHTML,
                customerGroup: row.cells[6].innerHTML,
                totalAmount: row.cells[7].innerHTML,
                externalReference: row.cells[8].innerHTML,
                docStatus:  row.cells[9].innerHTML,
                cancelReason: row.cells[10].innerHTML,
                creationDate:  row.cells[11].innerHTML,
                requestor:  row.cells[12].innerHTML
            }
            selectedSalesOrders.push(data)
        }
    }
}


// Populate Sales Order Table
function fetchSalesOrder() {
    if (localStorage.getItem("isApprover") === "true") {
        fetchSalesOrderApprover()
    } else {
        fetchSalesOrderRequestor()
    }
}

function fetchSalesOrderApprover() {
    fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/status/allForApprovalStatus`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        } 
        
    }).then(res => res.json()).then(data => {
        console.log("data: ",data);
        if (data.length < 1){
            salesOrder = `<h1 style="text-align: center;">No For Approval Sales Quotes Available</h1>`
            salesOrderTable.innerHTML = salesOrder;
        } else {
            salesOrder = data.map(result=> {
                let totalAmountPerSo = 0
                result.items.forEach(item => {
                    let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                    totalAmountPerSo += amountPerLine
                })
                return(
                    `
                    <tr>
                        <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                        <td style="text-align: center;">${result.sapSoId}</td>
                        <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                        <td style="text-align: center;">${result.accountId}</td>
                        <td style="text-align: center;">${result.accountName}</td>
                        <td style="text-align: center;">${result.salesGroup}</td>
                        <td style="text-align: center;">${result.customerGroup}</td>
                        <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                        <td style="text-align: center;">${result.externalReference}</td>
                        <td style="text-align: center;">${result.docStatus}</td>
                        <td style="text-align: center;">${result.cancelReason}</td>
                        <td style="text-align: center;">${result.creationDate}</td>
                        <td style="text-align: center;">${result.requestor}</td>
                    </tr>
                    `
                )
            }).join("")
            salesOrderTable.innerHTML = salesOrder;   
                               
        }
    })
}
    
function fetchSalesOrderRequestor() {
    fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => res.json()).then(data => {
        if (data.length < 1){
            salesOrder = `<h1 style="text-align: center;">No Sales Quotes Available</h1>`
            salesOrderTable.innerHTML = salesOrder;
        } else {
            salesOrder = data.map(result=> {
                let totalAmountPerSo = 0
                result.items.forEach(item => {
                    let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                    totalAmountPerSo += amountPerLine
                })
                return(
                    `
                    <tr>
                        <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                        <td style="text-align: center;">${result.sapSoId}</td>
                        <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                        <td style="text-align: center;">${result.accountId}</td>
                        <td style="text-align: center;">${result.accountName}</td>
                        <td style="text-align: center;">${result.salesGroup}</td>
                        <td style="text-align: center;">${result.customerGroup}</td>
                        <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                        <td style="text-align: center;">${result.externalReference}</td>
                        <td style="text-align: center;">${result.docStatus}</td>
                        <td style="text-align: center;">${result.cancelReason}</td>
                        <td style="text-align: center;">${result.creationDate}</td>
                        <td style="text-align: center;">${result.requestor}</td>
                    </tr>
                    `
                )
            }).join("")
            salesOrderTable.innerHTML = salesOrder;                        
        }
    })
}

fetchSalesOrder();


// Change of Filter by Status
function statusFilterCallback() {
    let statusFilterValue = document.getElementById('filterStatus').value

    if(statusFilterValue === 'allStatusRequestor') {
        fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/allSalesOrderinDatabase`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${result.salesGroup}</td>
                            <td style="text-align: center;">${result.customerGroup}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.cancelReason}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("")
                salesOrderTable.innerHTML = salesOrder;              
            }
        })
    } else if(statusFilterValue === 'inPreparationStatusRequestor') {
        fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/allInPrepStatus`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No In Preparation Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${result.salesGroup}</td>
                            <td style="text-align: center;">${result.customerGroup}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.cancelReason}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'forApprovalStatusRequestor') {
        fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/status/allForApprovalStatus`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No For Approval Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${result.salesGroup}</td>
                            <td style="text-align: center;">${result.customerGroup}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.cancelReason}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'postedStatusRequestor') {
        fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/status/posted/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Posted Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${result.salesGroup}</td>
                            <td style="text-align: center;">${result.customerGroup}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.cancelReason}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'rejectedStatusRequestor') {
        fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/status/allRejected`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Rejected Sales Quotes Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${result.salesGroup}</td>
                            <td style="text-align: center;">${result.customerGroup}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.cancelReason}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    } else if(statusFilterValue === 'cancelStatusRequestor') {
        fetch(`https://arcane-caverns-35746.herokuapp.com/api/salesOrder/status/cancelled/${employeeId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            if (data.length < 1){
                salesOrder = `<h1 style="text-align: center;">No Cancelled Sales Quote Available</h1>`
                salesOrderTable.innerHTML = salesOrder;
            } else {
                salesOrder = data.map(result=> {
                    let totalAmountPerSo = 0
                    result.items.forEach(item => {
                        let amountPerLine = parseFloat(item.taxAmount) + parseFloat(item.netValue)
                        totalAmountPerSo += amountPerLine
                    })
                    return(
                        `
                        <tr>
                            <td style="width: 8%"><input type="checkbox" id="flexCheckDefault">Select</td>
                            <td style="text-align: center;">${result.sapSoId}</td>
                            <td style="text-align: center;"><a href='salesOrderViewing.html?documentId=${result.salesOrderNo}'>${result.salesOrderNo}</a></td>
                            <td style="text-align: center;">${result.accountId}</td>
                            <td style="text-align: center;">${result.accountName}</td>
                            <td style="text-align: center;">${result.salesGroup}</td>
                            <td style="text-align: center;">${result.customerGroup}</td>
                            <td style="text-align: center;">${(totalAmountPerSo).toFixed(2)}</td>
                            <td style="text-align: center;">${result.externalReference}</td>
                            <td style="text-align: center;">${result.docStatus}</td>
                            <td style="text-align: center;">${result.cancelReason}</td>
                            <td style="text-align: center;">${result.creationDate}</td>
                            <td style="text-align: center;">${result.requestor}</td>
                        </tr>
                        `
                    )
                }).join("");
                salesOrderTable.innerHTML = salesOrder;                       
            }
        })
    }
}

document.getElementById('filterStatus').onchange = function() {
	statusFilterCallback()
};

