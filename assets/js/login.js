let loginForm = document.querySelector("#loginForm");

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let userName = document.getElementById("username").value;
	let password = document.getElementById("password").value;

	if (userName == "" || password == ""){
		alert("Please input your username and/or password.")
	} else { 
		fetch(`https://arcane-caverns-35746.herokuapp.com/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userName: userName.toUpperCase(),
				password: password
			})
		}).then(res => res.json()).then(data => {
			if(data.accessToken) {
				localStorage.setItem('token', data.accessToken);
				fetch(`https://arcane-caverns-35746.herokuapp.com/api/users/details`, {
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${data.accessToken}`
					}
				}).then(res => res.json()).then(data => {
					if(data.addOn === 'SO') {
						localStorage.setItem("id", data._id);
						localStorage.setItem("userName", data.userName);
						localStorage.setItem("isAdmin", data.isAdmin);
						localStorage.setItem("department", data.department);
						localStorage.setItem("employeeId", data.employeeId);
						localStorage.setItem("isApprover", data.isApprover);
						localStorage.setItem("isRequestor", data.isRequestor);

						if (data.initialLogin == true) {
							alert ("Initial Login Must Change Update the Password")
							window.location.replace("./changepassword.html")
						} else {
							alert (`Welcome ${data.firstName} ${data.lastName}`)
							window.location.replace("./launchpad.html")
						}
					} else {
						alert("Sorry! You don't have access for this Add On")
					}
				})
			} else {
				alert("Incorrect Username and/or Password");
			}
		})
	}
})
