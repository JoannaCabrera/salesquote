const mongoose = require('mongoose')

const tpsSchema = new mongoose.Schema({
    code: {
        type: String,
        require: true
    },
    vendorAccount: {
        type: String,
        require: true
    }
})

module.exports = mongoose.model('TPS', tpsSchema);