const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    employeeId:{
        type: String,
        required: true
    },
    userName: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isApprover: {
        type: Boolean,
        default: false
    },
    isRequestor: {
        type: Boolean,
        default: true
    },
    department: {
        type: String
    },
    addOn: {
        type: String,
        default: 'Sales Order'
    }
})
module.exports = mongoose.model('user', userSchema);