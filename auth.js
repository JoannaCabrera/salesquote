const jwt = require('jsonwebtoken')
const secret = 'ManilaBayThreadVendorPortal'

module.exports.createAccessToken = (result) => {
	const data = {
		id: result._id,
		employeeId: result.employeeId,
		userName: result.userName,
		isApprover: result.isApprover,
		isRequestor: result.isRequestor,
		isAdmin: result.isAdmin,
		department: result.department,
		application: result.application
	}

	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			return (err) ? res.send({ auth: 'failed' }) : next()
		})
	} else {
		return res.send({ auth: 'failed' })
	}
}

module.exports.decode = (token) => {
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, { complete: true }).payload
		})
	} else {
		return null
	}
}